import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from vk_api.upload import VkUpload
import random
import uuid
import models
from models import *
import json
import datetime
import openpyxl as opxl

SECKET_KEY = "fd104dc6f69ec26f030d8b678c58e5e51e8586664ab199a0b97a152ef1aa300e4693d53f3a5f93ed31c39"
STATE_WAIT = 0
STATE_READY = 1
STATE_DELETE_LIST = 2
STATE_3 = 3
STATE_4 = 4
#
# def activate_keyboard():
#     keyboard = VkKeyboard(one_time=True)
#     keyboard.add_button("Введите код активации:", color=VkKeyboardColor.POSITIVE)
#     keyboard.add_line()
#     keyboard.add_button("Удалить лист", color=VkKeyboardColor.NEGATIVE)
#
#     return keyboard

a_data = json.loads(open('autobus.json', 'r').read())
t_data = json.loads(open('trollebus.json', 'r').read())
m_data = json.loads(open('mtaxi.json', 'r').read())
m_data['16т'] = [
    "Вокзальная площадь",
    "улица Луначарского",
    "Крестовая улица",
    "проспект Ленина",
    "проспект Серова",
    "Гражданская улица",
    "улица Бабушкина",
    "Переборский тракт",
    "Целинная улица",
    "Шекснинское шоссе",
    "улица Рокоссовского",
    "Микрорайон ГЭС-14"
]
print(m_data)

def accept_dialog_keyboard():
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button("Да", color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Нет", color=VkKeyboardColor.NEGATIVE)
    return keyboard


vk_session = vk_api.VkApi(token=SECKET_KEY)
vk = vk_session.get_api()
longpoll = VkLongPoll(vk_session)

users = {

}

expectation = {}


def user_licence(user_id):
    global users
    if not (user_id in users):
        users[user_id] = {
            "state": STATE_WAIT,
            "list_info": {

            }
        }


tmp1 = str(datetime.datetime.now())

print(tmp1[11:16])

models.db.connect()


# models.db.create_tables([ models.License, models.List, models.Stop])
# for i in range(0, 200):
#     key = LicenseKey(key=uuid.uuid4().hex)
#     key.save()
# l = License(user_id=str(354545355), state=1)
# l.save()
str()
#
# def state_admin(event):
#     if event.user_id == 354545355:
#         if event.text == "key":
#             key = LicenseKey(key=uuid.uuid4().hex)
#             key.save()
#             text = key.key
#             vk.messages.send(
#                 user_id=event.user_id,
#                 random_id=random.random(),
#                 message=text,
#             )
#             return True
#     return False

#
# def state_0(event):
#     """
#
#     :param event:
#     :return:  True - если можно проверять другие состояния
#     """
#     us = list(License.select().where(License.user_id == str(event.user_id)))
#     if us == []:
#         lk = list(LicenseKey.select().where(LicenseKey.key == event.text))
#
#         if lk == []:
#             text = "Введите корректный ключ доступа!"
#             vk.messages.send(
#                 user_id=event.user_id,
#                 random_id=random.random(),
#                 message=text,
#             )
#             return False
#         else:
#             if License.select().where(License.key_id == lk[0].id).count() > 0:
#                 text = "Ключ уже активирован"
#                 vk.messages.send(
#                     user_id=event.user_id,
#                     random_id=random.random(),
#                     message=text,
#                 )
#                 return False
#             lic = License(
#                 user_id=str(event.user_id),
#                 key_id=lk[0],
#                 state=1
#             )
#             lic.save()
#             text = "Ключ доступа верный, можете начать работу!"
#             newlist = VkKeyboard(one_time=True)
#             newlist.add_button("Создать новый маршрутный лист", color=VkKeyboardColor.POSITIVE)
#             vk.messages.send(
#                 user_id=event.user_id,
#                 random_id=random.random(),
#                 message=text,
#                 keyboard=newlist.get_keyboard(),
#             )
#             return True
#     else:
#
#         return True


def state_1(event):
    license = License.get(License.user_id == str(event.user_id))

    # print(license)
    if event.text == "Создать новый маршрутный лист":
        text = "Введите тип транспорта"
        keyboard = VkKeyboard(one_time=True)
        keyboard.add_button("Автобус", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("Троллейбус", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("Маршрутка", color=VkKeyboardColor.PRIMARY)
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=keyboard.get_keyboard(),
        )
        license.state = 7
        license.save()
    else:
        text = "Вы имеете доступ "
        newlist = VkKeyboard(one_time=True)
        newlist.add_button("Создать новый маршрутный лист", color=VkKeyboardColor.POSITIVE)
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=newlist.get_keyboard(),
        )


def state_7(event):
    license = License.get(License.user_id == str(event.user_id))
    if event.text in ["Автобус", "Троллейбус", "Маршрутка"]:
        if event.text == "Автобус":
            license.transport_type = 0
            license.save()
            keys = a_data.keys()
            keyboard = VkKeyboard(one_time=True)
            c = 0
            for key in keys:

                if c == 4:
                    keyboard.add_line()
                    c = 0
                c += 1
                keyboard.add_button(key, color=VkKeyboardColor.PRIMARY)
            text = "Выберите номер автобуса"
            vk.messages.send(
                user_id=event.user_id,
                random_id=random.random(),
                message=text,
                keyboard=keyboard.get_keyboard(),
            )
            license.state = 9
            license.save()
        elif event.text == "Троллейбус":
            license.transport_type = 1
            license.save()
            keys = t_data.keys()
            keyboard = VkKeyboard(one_time=True)
            c = 0
            for key in keys:
                if c == 4:
                    keyboard.add_line()
                    c = 0
                c += 1
                keyboard.add_button(key, color=VkKeyboardColor.PRIMARY)
            text = "Выберите номер троллейбуса"
            vk.messages.send(
                user_id=event.user_id,
                random_id=random.random(),
                message=text,
                keyboard=keyboard.get_keyboard(),
            )
            license.state = 9
            license.save()
        elif event.text == "Маршрутка":
            license.transport_type = 2
            license.save()
            keys = m_data.keys()
            keyboard = VkKeyboard(one_time=True)
            c = 0
            for key in keys:
                if c == 4:
                    keyboard.add_line()
                    c = 0
                c += 1
                keyboard.add_button(key, color=VkKeyboardColor.PRIMARY)
            text = "Выберите номер маршрутки"
            vk.messages.send(
                user_id=event.user_id,
                random_id=random.random(),
                message=text,
                keyboard=keyboard.get_keyboard(),
            )
            license.state = 9
            license.save()





    else:
        text = "Тип транспорта не верный, начните с начала!"
        newlist = VkKeyboard(one_time=True)
        newlist.add_button("Создать новый маршрутный лист", color=VkKeyboardColor.POSITIVE)
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=newlist.get_keyboard(),
        )
        license.state = 1
        license.save()


def do_keyboard():
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button("Ввести вместительность (ВРУЧНУЮ!)", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("Добавить информацию об остановке", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("Закончить маршрут", color=VkKeyboardColor.PRIMARY)
    return keyboard


def state_9(event):
    try:
        license = License.get(License.user_id == str(event.user_id))
        print(event.text)
        license.transport_way = event.text

        license.state = 2
        license.save()
        date = datetime.datetime.now()
        current_list = []
        cl = List.select().where(List.user_id == license.user_id)
        for cli in cl:
            cli.delete_instance()
            # current_list.append(cli)
        # print("CL", current_list)
        # List.delete().where(str(List.user_id) == str(license.user_id))
        # List.save()

        list = List(user_id=license.user_id, name=str(license.transport_way), date=date, start=datetime.datetime.now(),
                    end=datetime.datetime.now())
        list.save()
        text = "Выберите действие"
        keyboard = do_keyboard()
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=keyboard.get_keyboard(),
        )
    except:
        license = License.get(License.user_id == str(event.user_id))
        newlist = VkKeyboard(one_time=True)
        newlist.add_button("Создать новый маршрутный лист", color=VkKeyboardColor.POSITIVE)
        text = "Вы ошиблись, начните заного"
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=newlist.get_keyboard(),
        )
        license.state = 1
        license.save()


def state_3(event):
    license = License.get(License.user_id == str(event.user_id))
    keyboard = VkKeyboard()
    keyboard.add_button("0", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("1", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("2", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("3", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("4", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("5", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("6", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("7", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("8", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("9", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("10", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("11", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("12", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("13", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("14", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("15", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("16", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("17", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("18", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("19", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("20", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("21", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("22", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("23", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("24", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("25", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("26", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("27", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("28", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("29", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("30", color=VkKeyboardColor.PRIMARY)
    keyboard.add_button("31", color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button("Столько не бывает :)", color=VkKeyboardColor.PRIMARY)
    text = "Укажите сколько человек вышло:"
    vk.messages.send(
        user_id=event.user_id,
        random_id=random.random(),
        message=text,
        keyboard=keyboard.get_keyboard(),
    )
    stop = Stop(name=event.text, ins=0, outs=0, num=0, list=List.get(List.user_id == license.user_id),
                time=datetime.datetime.now())
    stop.save()
    license.tmp_stop_id = stop.id
    license.state = 4
    license.save()


def state_4(event):
    license = License.get(License.user_id == str(event.user_id))
    if event.text in [str(x) for x in range(0, 32)]:
        stop = Stop.get(Stop.id == license.tmp_stop_id)
        stop.outs = int(event.text)
        stop.save()

        keyboard = VkKeyboard()
        keyboard.add_button("0", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("1", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("2", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("3", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("4", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("5", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("6", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("7", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("8", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("9", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("10", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("11", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("12", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("13", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("14", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("15", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("16", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("17", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("18", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("19", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("20", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("21", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("22", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("23", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("24", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("25", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("26", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("27", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("28", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("29", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("30", color=VkKeyboardColor.PRIMARY)
        keyboard.add_button("31", color=VkKeyboardColor.PRIMARY)
        keyboard.add_line()
        keyboard.add_button("Столько не бывает :)", color=VkKeyboardColor.PRIMARY)
        text = "А теперь укажите сколько человек вошло:"
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=keyboard.get_keyboard(),
        )
        license.state = 5
        license.save()
    else:
        license = License.get(License.user_id == str(event.user_id))
        keyboard = do_keyboard()
        text = "Вы ошиблись, выберите другую команду"
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=keyboard.get_keyboard(),
        )
        license.state = 2
        license.save()


def state_5(event):
    license = License.get(License.user_id == str(event.user_id))
    if event.text in [str(x) for x in range(0, 32)]:
        stop = Stop.get(Stop.id == license.tmp_stop_id)
        stop.ins = int(event.text)
        stop.time = datetime.datetime.now().time()
        stop.save()

        keyboard = do_keyboard()
        text = "Информация сохранена, можете выбрать другую команду"
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=keyboard.get_keyboard(),
        )
        license.state = 2
        license.save()
    else:
        license = License.get(License.user_id == str(event.user_id))
        keyboard = do_keyboard()
        text = "Вы ошиблись, выберите другую команду"
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=keyboard.get_keyboard(),
        )
        license.state = 2
        license.save()


def state_2(event):
    license = License.get(License.user_id == str(event.user_id))

    if event.text == "Ввести вместительность (ВРУЧНУЮ!)":
        license.state = 8
        license.save()

        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message="Введите вместительность (сидячие места)",
        )
    elif event.text == "Добавить информацию об остановке":

        keyboard = VkKeyboard(one_time=True)
        if license.transport_type == str(0):
            info = a_data
        elif license.transport_type == str(1):
            info = t_data
        else:
            info = m_data
        c = 0
        for stop in info[str(license.transport_way)]:
            if c == 4:
                keyboard.add_line()
                c = 0
            c += 1
            keyboard.add_button(stop, color=VkKeyboardColor.PRIMARY)
        # keyboard.add_button("<список остановок>", color=VkKeyboardColor.POSITIVE)
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message="Введите название",
            keyboard=keyboard.get_keyboard(),
        )
        license.state = 3
        license.save()
    elif event.text == "Закончить маршрут":
        newstp = VkKeyboard(one_time=True)
        newstp.add_button("Да", color=VkKeyboardColor.POSITIVE)
        newstp.add_line()
        newstp.add_button("Нет", color=VkKeyboardColor.NEGATIVE)
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message="Вы уверены?",
            keyboard=newstp.get_keyboard(),
        )
        license.state = 6
        license.save()
    else:
        license = License.get(License.user_id == str(event.user_id))
        keyboard = do_keyboard()
        text = "Вы ошиблись, выберите другую команду"
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message=text,
            keyboard=keyboard.get_keyboard(),
        )
        license.state = 2
        license.save()


def state_8(event):
    license = License.get(License.user_id == str(event.user_id))
    list = List.get(List.user_id == str(event.user_id))
    if str(event.text).isnumeric():
        list.seat_capacity = event.text
        list.save()
        license.state = 10
        license.save()
        keyboard = do_keyboard()
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message="Введите вместительность (МАКСИМАЛЬНАЯ)",
            # keyboard=keyboard.get_keyboard(),
        )
    else:
        keyboard = do_keyboard()
        license.state = 2
        license.save()
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message="Ошибка ввода, можете продолжить работу",
            keyboard=keyboard.get_keyboard(),
        )


def state_10(event):
    license = License.get(License.user_id == str(event.user_id))
    list = List.get(List.user_id == str(event.user_id))
    if str(event.text).isnumeric():
        list.max_capacity = event.text
        list.save()
        license.state = 2
        license.save()
        keyboard = do_keyboard()
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message="Вместимость записана, можете продолжить работу",
            keyboard=keyboard.get_keyboard(),
        )
    else:
        keyboard = do_keyboard()
        license.state = 2
        license.save()
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message="Ошибка ввода, можете продолжить работу",
            keyboard=keyboard.get_keyboard(),
        )


def loading_in_xls(event):
    wb = opxl.load_workbook("template.xlsx")
    us = list(License.select().where(License.user_id == str(event.user_id)))
    us = us[0]
    page = wb["page1"]

    lst = list(List.select().where(List.user_id == str(event.user_id)))
    lst = lst[0]
    if us.transport_type == '0':
        type = "Автобус"
    elif us.transport_type == '1':
        type = "Троллейбус"
    else:
        type = "Маршрутка"

    page["A1"] = "Маршрут №" + us.transport_way + f"({type})"
    day = datetime.datetime.now().day
    if (day == 27) or (day == 28):
        page["B2"] = "Выходной"
    else:
        page["B2"] = "Будний"
    page["D1"] = "Сид.: " + str(lst.seat_capacity) + " / Max: " + str(lst.max_capacity)

    sum = 0
    i = 5
    stops = Stop.select().where(Stop.list == lst)
    # if stops.count()< 2:
    lst.end = datetime.datetime.now().time()
    # else:
    #     lst.start = stops[0].time
    #     lst.end = stops[-1].time
    start = datetime.time.strftime(lst.start, "%H:%M")
    end = datetime.time.strftime(lst.end, "%H:%M")
    page["A3"] = "Начало движения: " + start + ", Конец движения: " + end
    for stop in stops:
        page["A" + str(i)] = str(i - 4)
        page["B" + str(i)] = stop.name
        # tmp2 = stop.time
        page["C" + str(i)] = datetime.time.strftime(stop.time, "%H:%M")
        page["D" + str(i)] = str(stop.ins)
        page["E" + str(i)] = str(stop.outs)
        sum += stop.ins - stop.outs
        page["F" + str(i)] = sum
        if int(lst.max_capacity) == 0.0:
            page["G" + str(i)] = 0
        else:
            page["G" + str(i)] = (sum / int(lst.max_capacity)) * 100
        i += 1
    wb.save(f"docs/list_{str(event.user_id)}.xlsx")


def state_6(event):
    license = License.get(License.user_id == str(event.user_id))

    if event.text == "Да":
        license.state = 1
        license.save()
        newlist = VkKeyboard(one_time=True)
        newlist.add_button("Создать новый маршрутный лист", color=VkKeyboardColor.POSITIVE)
        upload = VkUpload(vk)
        loading_in_xls(event)
        doc = upload.document_message(f"docs/list_{str(event.user_id)}.xlsx", "Маршрутный лист", peer_id=event.peer_id)
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message="Можете скачать свой маршрутный лист в формате .xlsx",
            attachment=[f"doc{doc['doc']['owner_id']}_{doc['doc']['id']}"],
            keyboard=newlist.get_keyboard(),
        )
    else:
        keyboard = do_keyboard()
        vk.messages.send(
            user_id=event.user_id,
            random_id=random.random(),
            message="Можете продолжить работу",
            keyboard=keyboard.get_keyboard(),
        )
        license.state = 2
        license.save()
        # vk.messages.send(
        #     user_id=event.user_id,
        #     random_id=random.random(),
        #     message="Можете продолжить работу",
        #     keyboard=newlist.get_keyboard(),
        # )


while True:
    # try:
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            # Check keyboard
            # text = "Введите ваш ключ доступа"

            # access = state_0(event)
            # if state_admin(event):
            #     continue
            # if not access:
            #     continue
            license = License.select().where(License.user_id == str(event.user_id))
            if license.count() < 1:
                license = License(
                    user_id=str(event.user_id),
                    state=1
                )
                license.save()
            else:
                license = license[0]
            if license.state == 1:
                state_1(event)
            elif license.state == 7:
                state_7(event)
            elif license.state == 9:
                state_9(event)
            elif license.state == 2:
                state_2(event)
            elif license.state == 8:
                state_8(event)
            elif license.state == 6:
                state_6(event)
            elif license.state == 3:
                state_3(event)
            elif license.state == 4:
                state_4(event)
            elif license.state == 5:
                state_5(event)
            elif license.state == 10:
                state_10(event)
            print("kek")
# except:
#     continue


models.db.close()
