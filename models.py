from peewee import *

db = SqliteDatabase('bot.db', pragmas=(('foreign_keys', 'on'),))


# class LicenseKey(Model):
#     key = CharField()
#
#     class Meta:
#         database = db

class License(Model):
    user_id = CharField(unique=True)
    # key_id = ForeignKeyField(LicenseKey)
    state = IntegerField(default=0)
    transport_type = CharField(default=0) # 0 - автобус, 1 - троллейбус, 2 - маршрутка
    transport_way = CharField(default=0) # номер маршрута
    # list = ForeignKeyField(List)
    tmp_stop_id = IntegerField(default=0)
    class Meta:
        database = db


class List(Model):  # Маршрутный лист
    user_id = CharField()
    name = CharField()
    date = DateField()
    seat_capacity = CharField(default=0)  # Вместительность
    max_capacity = CharField(default=0)  # Вместительность
    start = TimeField()
    end = TimeField()

    class Meta:
        database = db



class Stop(Model):
    id = AutoField()
    name = CharField()  # название
    ins = IntegerField()  # вошло
    outs = IntegerField()  # вышло
    num = IntegerField()  # количество
    list = ForeignKeyField(List, on_delete='CASCADE')  # принадлежность к листу
    time = TimeField()
    class Meta:
        database = db


