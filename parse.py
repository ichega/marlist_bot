import bs4
import requests
import json

html = open('mtaxi.html', "r").read()

soup = bs4.BeautifulSoup(html, 'html.parser')
s = soup.find('div',  attrs={"class": "entry-content"})
# print(s)
s = soup.find_all('div', attrs={"class": "boxrasp1"},recursive=True)
rasps = []
for i in s:
    try:
        # print(i.h3.span)
        if "Маршрут" in str(i.h3.span):
            rasps.append(i)
    except:
        continue

print(len(rasps))
stops = {}

for r in rasps:
    print(r.attrs)
    if "close_route_d" in r.attrs["class"]:
        continue
    s = r.ul.li.ul.li
    # print(r.h3.span.text)
    idx = str(r.h3.span.text).replace("Маршрут № ", "")
    print(idx)
    stops[idx] = []

    stop_a = s.find_all('a')
    for stop in stop_a:
        stops[idx].append(stop.text)
print(stops)
s = json.dumps(stops)
with open("mtaxi.json", "w") as f:
    f.write(s)

print(stops['1'])